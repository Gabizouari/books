
		(function() {
			var article = $('#firstArticle'),
				article2 = $('#secondArticle'),
				def= $('#defaultArticles'),
				articles = $('article');
			
			article.on('click',function(e){
				articles.css('display','none');
				$('#fullfirstarticle').fadeIn(1000);
				
				def.parents().removeClass('active');
				article2.parent().removeClass('active');
				article.parent().addClass('active');
				
				e.preventDefault();
				
			});
			
			article2.on('click',function(e){
				articles.css('display','none');
				$('#fullsecondarticle').fadeIn(1000);
				
				def.parent().removeClass('active');
				article.parent().removeClass('active');
				article2.parent().addClass('active');
				
				e.preventDefault();
				
			});
			
			def.on('click',function(e){
				articles.fadeIn(1000);
				$('#fullfirstarticle').css('display','none');
				$('#fullsecondarticle').css('display','none');
				
				article.parent().removeClass('active');
				article2.parent().removeClass('active');
				def.parent().addClass('active');
				
				e.preventDefault();
				
			});
		})();
