<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Authentification  
 *
 * treat the connexion depending on the profil logged in  
 *
 * @author		Sabri Zouari
 */

class Authentification extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('authentification_model');
	}
	
	public function index()
	{
		$this->load->view('authentification/index');
	}
	
   /**
	*
	* @desc create new user
	* @var string email 
	* @var string password
	* @return void
	*
	*/
	
	public function create()
	{
		// get vars
		$name = $this->input->post('name');
		$phone = $this->input->post('phone');
		$taxId = $this->input->post('tax_id');
		$login = $this->input->post('email');
		$pass = md5($this->input->post('password'));
		
		// check if mail already exist
		$check = $this->_checkEmail($login);
		
		if( $check !== FALSE) {
				
			$data = array(  
							'name' 	   => $name,
							'phone'    => $phone,
							'tax_id'    => $taxId,
							'username' => $login,
							'pass' 	   => $pass,
							'profil' 	   => 'user'
						);	
			
			// create user or return authentification form
			( isset($login) AND isset($pass) ) ? $queryAdd = $this->authentification_model->create($data) : FALSE;  
			
			if($queryAdd!==FALSE) 
			{
				$data['result'] = "Account created successfully !" ; 
				$data['className'] = 'successMessage';
				$this->load->view('authentification/index',$data);	  	
			}else{
				$data['result'] = "problem occured during the creation !" ; 
				$data['className'] = 'errorMessage';
				$this->load->view('authentification/index',$data);	   
			}	
			
		}else{
			$data['result'] = "Email already exist !" ; 
			$data['className'] = 'errorMessage';
			$this->load->view('authentification/index',$data);	  
		}	
		
	}
	
	private function _checkEmail($email)
	{
		$query = $this->authentification_model->checkEmail($email);
		return $query;
	}
	
   /**
	*
	* @desc check credentials in order to process the authentification
	* @var string email 
	* @var string password
	* @return boolean 
	*
	*/
	
	function validate_credentials()
	{		
		
		// get vars
		$login = $this->input->post('login');
		$pass = $this->input->post('pass');
		
		// check credentials 
		( isset($login) AND isset($pass) ) ? $query = $this->authentification_model->validate() : FALSE;   
		
		if($query!==FALSE) 
		{
			 foreach ($query->result() as $row)
                {
			        // create session data
					$data = array(  'id'     => $row->id, 
									'login'  => $this->input->post('login'),
									'profil'  => $row->profil,
                                    'is_logged_in' => true 
                                    );
									
					// save session 	
				    $this->session->set_userdata($data);  
				   
					redirect('books');  
					                 
                }
		}else {
			$data['error_authentification'] = "Login or password failed !" ; 
			$this->load->view('authentification/index',$data);	 
	
		}
	}

   /**
	*
	* @desc destroy session vars and redirection to authentification form 
	* @return void
	*
	*/	
	
    function logout() 
	{
		$this->session->sess_destroy();
		redirect ('authentification');    
	}

}
