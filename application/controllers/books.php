 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * books management File 
 *
 * display  books
 *
 * @author		Sabri Zouari
 */

class Books extends CI_Controller {

	function __construct()
	{
        parent::__construct();
       	$this->load->model('books_model');
		$this->load->model('images');
    } 
	
   /**
    *
	* @desc display list of books 
	* @var void
	* @return array of objects
	*
	*/
	
	public function index($action = '' )
	{
		if (!empty ($action)) {
			switch ($action) {
					case "add":
					   $message  = "Item added successfully";
						break;
					case "update":
						$message  = "Item updated successfully";
						break;
					case "delete":
						$message  = "Item deleted successfully";
						break;
			}
			$data['result']  = $message;
		}	
		
		$data['books'] = $this->books_model->get_books();
		$this->load->view('books/index',$data);
	}
	
	function mybooks()
	{
		$data['books'] = $this->books_model->get_loaned_books();
		$this->load->view('books/loaned',$data);
	}
	
	function loanedbooks()
	{
		$this->is_logged_in();
		$data['books'] = $this->books_model->get_loaned_books_admin();
		$this->load->view('books/loaned_list',$data);
	}
	
	public function create()
	{
		$this->is_logged_in();
		$this->load->view('books/add_book');
	}
	
	public function save()
	{
		$this->is_logged_in();
		$image = $this->images->do_upload();
		
		if ( ! $this->upload->do_upload())
		{
			$error =  $this->upload->display_errors();
			$data['error']  = $error;
			$this->load->view('books/add_book',$data);
			return;
		}
   				
	   		$data = array( 
						    'isbn'  	=> $this->input->post('isbn') ,
							'name'  	=> $this->input->post('name') ,
							'author'  	=> $this->input->post('author') ,
							'publisher'  	=> $this->input->post('publisher') ,
							'date_publication'  => $this->input->post('date_publication') ,
	   	                	'photo'     => $image,
						    'description'   => $this->input->post('description') 
						);
		
		$queryAdd = $this->books_model->create($data);  		
		
		if($queryAdd!==FALSE) 
			{
				$result = "Book created successfully !" ; 
				$this->index('add');	  	
			}else{
				$result = "problem occured during the creation !" ; 
				$className = 'errorMessage';
				$this->index($result , $className );		   
			}					
	}
	
	public function update(){
   		
		$this->is_logged_in();
		
		if ($this->input->post(NULL,true))
   		{
   			$image = $this->images->do_upload();
			
			($image=='') ? $image=$this->input->post('image'):  $image= $image; 	
   			
   			$data = array( 
						    'isbn'  	=> $this->input->post('isbn') ,
							'name'  	=> $this->input->post('name') ,
							'author'  	=> $this->input->post('author') ,
							'publisher'  	=> $this->input->post('publisher') ,
							'date_publication'  => $this->input->post('date_publication') ,
	   	                	'photo'     => $image,
						    'description'   => $this->input->post('description') 
						);
						
    		$this->books_model->update($data,$this->input->post('id'));
    		$this->index('update');
   		   
   		}else{
   			$data['book'] = $this->books_model->get_book();
   			$this->load->view('books/upd_book',$data);
   		}
    	
    	
   	}
	
	public function detail()
	{
		$data['book'] = $this->books_model->get_book();
   		$this->load->view('books/detail_book',$data);
	}
	
	public function loaned($id)
	{
		if($this->session->userdata('login')) 
		{
			$dateReturn = strtotime("+7 day");
			
			$data = array(  'loaned'  	=> 1 ,
							'date_loan'  	=> date('Y-m-d'),
							'date_return'  	=> date('Y-m-d',$dateReturn),
							'id_user'  	=> $this->session->userdata('id')
						);
			
			$this->books_model->update($data,$id);
			$this->index('update');	
		}else{
			redirect('books');
		}	
	}
	
	public function returnbook($id)
	{
		if($this->session->userdata('login')) 
		{
			
			$data = array(  'loaned'  	=> 0 ,
							'date_loan'  	=> '',
							'date_return'  	=> '',
							'id_user'  	=> 0
						);
			
			$this->books_model->update($data,$id);
			$this->index('update');	
		}else{
			redirect('books');
		}	
	}
	
	public function delete(){
		$this->is_logged_in();
   		$this->books_model->delete_book();
   		$this->index('delete');
   	}
	
   /**
	*
	* @desc XSS attack security , check if there is logged in session
	* @see controllers/authentification.php 
	* @return void					
	*/
	
	function is_logged_in()
	{
		$profil = $this->session->userdata('profil');
			
		if(!isset($profil) OR $profil != 'admin' )
		{		
			redirect('books');
			die(); 
		}		
	}	

}
