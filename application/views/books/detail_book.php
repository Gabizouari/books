<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Test assignement </title>
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url();?>/assets/css/bootstrap.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?php echo base_url();?>/assets/css/custom-backend.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>
	<script src="<?php echo base_url();?>/assets/js/respond.js"></script>
	<![endif]-->
</head>
<body>
	<div class="container">
		 <!-- Fixed navbar -->
		<div class="navbar navbar-inverse " role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			  <a class="navbar-brand" href="#">Book library</a>
			</div>
			<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<?php if ($this->session->userdata('profil')== 'admin') { ?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Books management <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><?php echo anchor ('books/create','Add Book' ); ?></li>
						<li><?php echo anchor ('books/','Books list' ); ?></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Users management <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
						<li><?php echo anchor ('users/','Users list' ); ?></li>
					</ul>
				</li>
				<li>
					<?php echo anchor ('books/loanedbooks','Loan management' ); ?>
				</li>
				<?php } elseif ($this->session->userdata('profil')== 'user') { ?>
				<li><?php echo anchor ('books/','Books list' ); ?></li>
				<li><?php echo anchor ('books/mybooks','Loaned Books list' ); ?></li>
				<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
				<?php }else{ ?>
				<li><?php echo anchor ('books/','Books list' ); ?></li>
				<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
				<?php } ?>
			</ul>
			<?php if ($this->session->userdata('login')) : ?> 
			<p class="navbar-text navbar-right">
				Signed in as <a href="#" class="navbar-link"><?php echo $this->session->userdata('login'); ?></a>
				<a href="<?php echo base_url()?>authentification/logout" class="btn btn-danger">Log out</a>
			</p>
			<?php endif;?>
			</div><!--/.nav-collapse -->
		</div>
		<?php foreach($book as $item) : ?>
			<h2> <?php echo $item->name;?></h2>
			<p class="text-right">
			<?php if ($this->session->userdata('login')) : 
					if($item->loaned== 0) : 
						echo anchor('Books/loaned/'.$this->uri->segment(3),'Loan checkout','class="btn btn-success"'); 
					else: 
						echo anchor('#','Book already loaned date return :' .$item->date_return,'class="btn btn-danger disabled"');  
					endif;
				endif;	
				echo anchor('Books/',' Back to list','class="btn btn-primary"'); ?>
			</p>
			<div class="row" >
				<label for="name" class="col-sm-2 control-label">Book Title</label>
				<div class="col-sm-10">
					<?php echo $item->name;?>
				</div>
			</div>
			<div class="row" >
				<label for="name" class="col-sm-2 control-label">ISBN-13</label>
				<div class="col-sm-10">
					<?php echo $item->isbn;?>
				</div>
			</div>	
			<div class="row" >
				<label for="name" class="col-sm-2 control-label">Author</label>
				<div class="col-sm-10">
					<?php echo $item->author;?>
				</div>
			</div>	
			<div class="row" >
				<label for="name" class="col-sm-2 control-label">Date publication</label>
				<div class="col-sm-10">
					<?php echo $item->date_publication;?>
				</div>
			</div>	
			<div class="row">
				<label for="photo" class="col-sm-2 control-label">Cover Photo</label>
				<div class="col-sm-10">
					<img src="<?php echo base_url().'assets/uploads/thumbs/'.$item->photo; ?>" />
				</div>
			</div>
			<div class="row">
				<label for="other" class="col-sm-2 control-label">Description</label>
				<div class="col-sm-10">
					<?php echo $item->description;?>
				</div>
			</div>	
		
		<?php endforeach;?>  
		</div>
	</div>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="<?php echo base_url();?>/assets/js/bootstrap.js" /> </script>
</body>
</html>