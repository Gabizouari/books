<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images extends CI_Model {
	
   var $images_paths;
	 
   function __construct()
    {
        parent::__construct();
        $this->images_path = realpath(APPPATH.'../assets/uploads');
        
    }
	
	function do_upload() {	
		
		$configs = array(
		  'allowed_types'  => 'jpg|jpeg|gif|png',
		  'upload_path'    => $this->images_path,
		  'max-width'  => 1024,
		  'max-height'  => 768,
		  'max-size'   => 2000
		);
		
		$this->load->library('upload',$configs);
		
				
		$this->upload->do_upload();
		$image_data = $this->upload->data();
		
		$configs = array(
		  'source_image'  => $image_data['full_path'],
		  'new_image'    => $this->images_path.'/thumbs',
		  'maintain_ration'    => TRUE,
		  'width'    => '500',
		  'height'    => '350'
		);
		
		$this->load->library('image_lib',$configs);
		$this->image_lib->resize();
		
		return $image_data['file_name'];
		
		
	}
	
	
	
}