-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mar 18 Février 2014 à 14:02
-- Version du serveur: 5.5.24-log
-- Version de PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `books`
--

-- --------------------------------------------------------

--
-- Structure de la table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `date_publication` date NOT NULL,
  `author` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `loaned` tinyint(11) NOT NULL,
  `date_loan` date NOT NULL,
  `date_return` date NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Contenu de la table `books`
--

INSERT INTO `books` (`id`, `isbn`, `name`, `photo`, `description`, `date_publication`, `author`, `publisher`, `loaned`, `date_loan`, `date_return`, `id_user`) VALUES
(35, '34fe', 'Book11', 'IMG_1901.JPG', 'content here qq', '2014-02-17', 'test author', 'pub test', 0, '0000-00-00', '0000-00-00', 0),
(36, '34feet', 'book2 ', 'putty-rm2.png', 'descrption here ...', '2014-02-18', 'test author', 'test publisher', 0, '0000-00-00', '0000-00-00', 0),
(37, 'isbn456', 'book3 ', 'putty-rm4.png', 'content here', '2014-02-21', 'author ', 'publisher', 1, '2014-02-18', '2014-02-03', 17),
(38, 'isbn456', 'book4', 'putty-rm6.png', 'content here', '2014-02-10', 'author ', 'publisher', 0, '0000-00-00', '0000-00-00', 0),
(40, 'isbn456', 'title  ', 'putty-rm10.png', 'content here', '2014-02-10', 'author ', 'publisher', 1, '2014-02-18', '2014-02-25', 18);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `tax_id` bigint(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `profil` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `tax_id`, `username`, `pass`, `profil`) VALUES
(16, 'Admin', '212121212121', 0, 'admin@domaine.com', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(17, 'sabri Zouari', '45454545', 2222221111, 'gabizouari@gmail.com', '098f6bcd4621d373cade4e832627b4f6', 'user'),
(18, 'sabri Zouari', '45456456456546', 2222221111, 'sabri.zouari@sqills.com', '098f6bcd4621d373cade4e832627b4f6', 'user');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
