<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Books_model extends CI_Model {
	
	  
   function __construct()
    {
        parent::__construct();
	}

	function create($data)
	{
		$insert = $this->db->insert('books', $data);
		return $insert;	
	}
	
	function get_books()
	{
		$this->db->order_by("id","desc");
		$query = $this->db->get('books');
		return $query->result();
	}
	
	function get_book()
	{	
	
		$query = $this->db->where('id', $this->uri->segment(3));
		$query = $this->db->get('books');
		return $query->result();	
	}
	
	function get_loaned_books()
	{	
		$query = $this->db->where('id_user', $this->session->userdata('id'));
		$query = $this->db->get('books');
		return $query->result();	
	}
	
	function get_loaned_books_admin()
	{	
		$query = $this->db->where('loaned', 1);
		$query = $this->db->get('books');
		return $query->result();	
	}
	
		
	function update($data,$id) 
	{	
		$query = $this->db->where('id', $id);
		$query = $this->db->update('books',$data);
		return;		
	}
	
	function delete_book()
	{	
		$this->db->where('id', $this->uri->segment(3));
		$this->db->delete('books');
		return;		
	}
	
	
}