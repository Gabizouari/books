<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Test assignement </title>
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url();?>/assets/css/bootstrap.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?php echo base_url();?>/assets/css/custom-backend.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>
	<script src="<?php echo base_url();?>/assets/js/respond.js"></script>
	<![endif]-->
</head>
<body>
	<div class="container">
		 <!-- Fixed navbar -->
		<div class="navbar navbar-inverse " role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			  <a class="navbar-brand" href="#">Book library</a>
			</div>
			<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<?php if ($this->session->userdata('profil')== 'admin') { ?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Books management <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><?php echo anchor ('books/create','Add Book' ); ?></li>
						<li><?php echo anchor ('books/','Books list' ); ?></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Users management <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
						<li><?php echo anchor ('users/','Users list' ); ?></li>
					</ul>
				</li>
				<li>
					<?php echo anchor ('books/loanedbooks','Loan management' ); ?>
				</li>
				<?php } elseif ($this->session->userdata('profil')== 'user') { ?>
				<li><?php echo anchor ('books/','Books list' ); ?></li>
				<li><?php echo anchor ('books/mybooks','Loaned Books list' ); ?></li>
				<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
				<?php }else{ ?>
				<li><?php echo anchor ('books/','Books list' ); ?></li>
				<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
				<?php } ?>
			</ul>
			<?php if ($this->session->userdata('login')) : ?> 
			<p class="navbar-text navbar-right">
				Signed in as <a href="#" class="navbar-link"><?php echo $this->session->userdata('login'); ?></a>
				<a href="<?php echo base_url()?>authentification/logout" class="btn btn-danger">Log out</a>
			</p>
			<?php endif;?>
			</div><!--/.nav-collapse -->
		</div>
		<?php foreach($user as $item) : ?>
			<h2> <?php echo $item->username;?></h2>
			<p class="text-right">
				<?php echo anchor('users/','Back to list','class="btn btn-primary"'); ?>
			</p>
			<div class="row" >
				<label for="name" class="col-sm-2 control-label">Name</label>
				<div class="col-sm-10">
					<?php echo $item->name;?>
				</div>
			</div>
			<div class="row" >
				<label for="name" class="col-sm-2 control-label">Phone</label>
				<div class="col-sm-10">
					<?php echo $item->phone;?>
				</div>
			</div>
			<div class="row" >
				<label for="name" class="col-sm-2 control-label">Social Security Code</label>
				<div class="col-sm-10">
					<?php echo $item->tax_id;?>
				</div>
			</div>
			<div class="row" >
				<label for="name" class="col-sm-2 control-label">Username</label>
				<div class="col-sm-10">
					<?php echo $item->username;?>
				</div>
			</div>
			
		
		<?php endforeach;?>  
		</div>
	</div>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="<?php echo base_url();?>/assets/js/bootstrap.js" /> </script>
</body>
</html>