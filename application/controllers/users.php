<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Users management
 *
 * display  users
 *
 * @author		Sabri Zouari
 */

class Users extends CI_Controller {

	function __construct()
	{
        parent::__construct();
		$this->is_logged_in();
       	$this->load->model('users_model');
	} 
	
   /**
    *
	* @desc display list of users 
	* @var void
	* @return array of objects
	*
	*/
	
	public function index()
	{
		$data['users'] = $this->users_model->get_users();
		$this->load->view('users/index',$data);
	}
	
		
	public function detail()
	{
		$data['user'] = $this->users_model->get_user();
   		$this->load->view('users/detail_user',$data);
	}
	
	
   /**
	*
	* @desc XSS attack security , check if there is logged in session
	* @see controllers/authentification.php 
	* @return void					
	*/
	
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
			
		if(!isset($is_logged_in) || $is_logged_in != true )
		{		
			if($this->session->userdata('profil') !=='admin') {
				redirect('books');
				die(); 
			}
		}		
	}	

}
