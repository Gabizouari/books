<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Test assignement </title>
	<!-- Bootstrap core CSS -->
	<link href="<?php echo base_url();?>/assets/css/bootstrap.css" rel="stylesheet">
	<!-- Custom styles for this template -->
	<link href="<?php echo base_url();?>/assets/css/custom.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="<?php echo base_url();?>/assets/js/html5shiv.js"></script>
	<script src="<?php echo base_url();?>/assets/js/respond.js"></script>
	<![endif]-->
</head>
<body>
	<div class="container">
		 <!-- Fixed navbar -->
		<div class="navbar navbar-inverse " role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			  <a class="navbar-brand" href="#">Book library</a>
			</div>
			<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<?php if ($this->session->userdata('profil')== 'admin') { ?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Books management <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><?php echo anchor ('books/create','Add Book' ); ?></li>
						<li><?php echo anchor ('books/','Books list' ); ?></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Users management <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
						<li><?php echo anchor ('users/','Users list' ); ?></li>
					</ul>
				</li>
				<li>
					<?php echo anchor ('books/loanedbooks','Loan management' ); ?>
				</li>
				<?php } elseif ($this->session->userdata('profil')== 'user') { ?>
				<li><?php echo anchor ('books/','Books list' ); ?></li>
				<li><?php echo anchor ('books/mybooks','Loaned Books list' ); ?></li>
				<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
				<?php }else{ ?>
				<li><?php echo anchor ('books/','Books list' ); ?></li>
				<li><?php echo anchor ('authentification','Create account / login' ); ?></li>
				<?php } ?>
			</ul>
			<?php if ($this->session->userdata('login')) : ?> 
			<p class="navbar-text navbar-right">
				Signed in as <a href="#" class="navbar-link"><?php echo $this->session->userdata('login'); ?></a>
				<a href="<?php echo base_url()?>authentification/logout" class="btn btn-danger">Log out</a>
			</p>
			<?php endif;?>
			</div><!--/.nav-collapse -->
		</div>
		<div class="row" >
		<?php if (!$this->session->userdata('login')) : ?>
			<form class="form-signin col-md-6" role="form"  method="post" action="<?php echo base_url();?>authentification/create">
				<h2 class="form-signin-heading">Be a Member</h2>
				<input type="text" class="form-control" name="name" placeholder="name"  >
				<input type="text" class="form-control" name="phone" placeholder="Phone"  required>
				<input type="text" class="form-control" name="tax_id" placeholder="Social security number YYMMDD0000" maxlength="10" required>
				<input type="email" class="form-control" name="email" placeholder="Email address" required >
				<input type="password" class="form-control" name="password" placeholder="Password (only letters allowed 4 <> 10)"  pattern="[A-Za-z]{4,10}" required>
				<button class="btn btn-lg btn-success btn-block" type="submit">Create account</button>
				<?php if (isset ($result)) : ?> 
					<div id="<?php echo $className; ?>" class="rounded"><?php echo $result;?></div>
				<?php endif;?>
			</form>
				
			<form class="form-signin col-md-6" role="form" method="post" action="<?php echo base_url();?>authentification/validate_credentials">
				<h2 class="form-signin-heading">Please sign in</h2>
				<input type="email" class="form-control" name="login" placeholder="Email address" required autofocus>
				<input type="password" class="form-control" name="pass" placeholder="Password" required>
				<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
				<?php if (isset ($error_authentification)) : ?> 
					<div id="errorMessage" class="rounded"><?php echo $error_authentification;?></div>
				<?php endif;?>
			</form>
			<?php else : ?>
				<h1>you're allredy logged in as <?php echo $this->session->userdata('login') ; ?> </h1>
			<?php endif;?>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="<?php echo base_url();?>/assets/js/bootstrap.js" /> </script>
</body>
</html>