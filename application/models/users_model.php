<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {
	
	  
   function __construct()
    {
        parent::__construct();
	}

	
	function get_users()
	{
		$this->db->order_by("id","desc");
		$query = $this->db->get('users');
		return $query->result();
	}
	
	function get_user()
	{	
	
		$query = $this->db->where('id', $this->uri->segment(3));
		$query = $this->db->get('users');
		return $query->result();	
	}
	
	
	
	
}